// soal 1
void main(List<String> args) {
  print(range(1, 10));
  print(range(10, 1));
  print(range(11, 18));
  print(range(18, 11));
}

range(startNum, finishNum){
  List <int> listNum = [];
  if(startNum > finishNum){
    for (var i = startNum; i >= finishNum; i--){
      listNum.add(i);
    }
    return listNum;
  }
  for (var i = startNum; i <= finishNum; i++){
    listNum.add(i);
  }
  return listNum;
}

// soal 2
// void main(List<String> args) {
//   print(rangeWithStep(1, 10, 2));
//   print(rangeWithStep(11, 23, 3));
//   print(rangeWithStep(5, 2, 1));
// }

rangeWithStep(startNum, finishNum, step){
  List <int> listNum = [];
  if(startNum > finishNum){
    for (var i = startNum; i >= finishNum; i-=step){
      listNum.add(i);
    }
    return listNum;
  }
  for (var i = startNum; i <= finishNum; i+=step){
    listNum.add(i);
  }
  return listNum;
}

// soal 3
// void main(List<String> args) {
//   var input = [
//     ['0001', 'Roman Alamsyah', 'Bandar Lampung', '21/05/1989', 'Membaca'],
//     ['0002', 'Dika Sembiring', 'Medan', '10/10/1992', 'Bermain Gitar'],
//     ['0003', 'Winona', 'Ambon', '25/12/1965', 'Memasak'],
//     ['0004', 'Bintang Senjaya', 'Martapura', '6/4/1970', 'Berkebun']
//   ];

//   dataHandling(input);
// }

dataHandling(data){
  for (var item in data) {
    print('Nomor ID: ' + item[0]);
    print('Nama Lengkap: ' + item[1]);
    print('TTL: ' + item[2] + ' ' + item[3]);
    print('Hobi: ' + item[4]);
    print('');
  }
}

// soal 4
// void main(List<String> args) {
//   print(balikKata('Kasur'));
//   print(balikKata('SanberCode'));
//   print(balikKata('Haji'));
//   print(balikKata('racecar'));
//   print(balikKata('Sanbers'));
// }

balikKata(word){
  var reservedWord = '';
  for (var i = word.length-1; i >= 0; i--) {
    reservedWord += word[i];
  }
  return reservedWord;
}