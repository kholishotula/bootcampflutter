// @dart=2.11

import 'dart:math';

import 'bangun_datar.dart';

class Segitiga extends Bangun_Datar{
  double alas;
  double tinggi;
  double miring;

  Segitiga (double a, double t){
    this.alas = a;
    this.tinggi = t;

    this.miring = sqrt(a*a + t*t);
  }

  @override
  double hitungLuas(){
    return 0.5 * this.alas * this.tinggi;
  }

  @override
  double hitungKeliling(){
    return this.alas + this.tinggi + this.miring;
  }
}
