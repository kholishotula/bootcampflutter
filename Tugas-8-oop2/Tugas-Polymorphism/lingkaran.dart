// @dart=2.11

import 'bangun_datar.dart';

class Lingkaran extends Bangun_Datar{
  double jari;
  double phi = 3.14;

  Lingkaran (double r){
    this.jari = r;
  }

  @override
  double hitungLuas(){
    return this.phi * this.jari * this.jari;
  }

  @override
  double hitungKeliling(){
    return 2 * this.phi * this.jari;
  }
}
