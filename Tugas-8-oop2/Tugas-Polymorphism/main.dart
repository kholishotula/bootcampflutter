// @dart=2.11

import 'bangun_datar.dart';
import 'lingkaran.dart';
import 'segitiga.dart';
import 'persegi.dart';

void main(List<String> args) {
  Bangun_Datar bangun_datar = Bangun_Datar();
  Lingkaran lingkaran = Lingkaran(10.0);
  Segitiga segitiga = Segitiga(3.0, 4.0);
  Persegi persegi = Persegi(6.0);

  bangun_datar.hitungKeliling();
  bangun_datar.hitungLuas();
  print('...........................');
  print('Lingkaran');
  print('Luas lingkaran dengan jari-jari 10 adalah ' + lingkaran.hitungLuas().toString());
  print('Keliling lingkaran dengan jari-jari 10 adalah ' + lingkaran.hitungKeliling().toString());
  print('===========================');
  print('Segitiga Siku-siku');
  print('Luas segitiga dengan alas 3 dan tinggi 4 adalah ' + segitiga.hitungLuas().toString());
  print('Keliling segitiga dengan alas 3 dan tinggi 4 adalah ' + segitiga.hitungKeliling().toString());
  print('===========================');
  print('Persegi');
  print('Luas persegi dengan sisi 6 adalah ' + persegi.hitungLuas().toString());
  print('Keliling persegi dengan sisi 6 adalah ' + persegi.hitungKeliling().toString());
}
