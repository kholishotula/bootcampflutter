// @dart=2.11

import 'bangun_datar.dart';

class Persegi extends Bangun_Datar{
  double sisi;

  Persegi (double s){
    this.sisi = s;
  }

  @override
  double hitungLuas(){
    return this.sisi * this.sisi;
  }

  @override
  double hitungKeliling(){
    return 4 * this.sisi;
  }
}
