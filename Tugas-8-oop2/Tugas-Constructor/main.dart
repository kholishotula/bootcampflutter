// @dart=2.11

import 'employee.dart';

void main(List<String> args) {
  var emp1 = new Employee.id(1);
  emp1.name = 'Kholishotul';
  emp1.department = 'Web';

  var emp2 = new Employee.name('Amaliah');
  emp2.id = 2;
  emp2.department = 'Android';

  var emp3 = new Employee.department('iOS');
  emp3.id = 3;
  emp3.name = 'Kholishotul Amaliah';

  print('Pegawai 1');
  print('ID : ' + emp1.id.toString());
  print('Name : ' + emp1.name);
  print('Department : ' + emp1.department);
  print('=================================');
  print('Pegawai 2');
  print('ID : ' + emp2.id.toString());
  print('Name : ' + emp2.name);
  print('Department : ' + emp2.department);
  print('=================================');
  print('Pegawai 3');
  print('ID : ' + emp3.id.toString());
  print('Name : ' + emp3.name);
  print('Department : ' + emp3.department);
}
