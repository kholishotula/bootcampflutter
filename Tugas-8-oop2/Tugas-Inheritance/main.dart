// @dart=2.11

import 'armor_titan.dart';
import 'attack_titan.dart';
import 'beast_titan.dart';
import 'human.dart';
import 'titan.dart';

void main(List<String> args) {
  Armor_Titan armor_titan = Armor_Titan();
  Attack_Titan attack_titan = Attack_Titan();
  Beast_Titan beast_titan = Beast_Titan();
  Human human = Human();
  Titan titan = Titan();

  armor_titan.setPowerPoint(5);
  attack_titan.setPowerPoint(10);
  beast_titan.setPowerPoint(6);
  human.setPowerPoint(1);
  titan.setPowerPoint(3);

  print('Armor Titan');
  print('Power point : ' + armor_titan.getPowerPoint().toString());
  print('Armor Titan sedang menerjang. ' + armor_titan.terjang());
  print('===========================');
  print('Attack Titan');
  print('Power point : ' + attack_titan.getPowerPoint().toString());
  print('Attack Titan sedang memukul. ' + attack_titan.punch());
  print('===========================');
  print('Beast Titan');
  print('Power point : ' + beast_titan.getPowerPoint().toString());
  print('Beast Titan sedang melempar. ' + beast_titan.lempar());
  print('===========================');
  print('Human');
  print('Power point : ' + human.getPowerPoint().toString());
  print('Human sedang membunuh semua Titan. ' + human.killAllTitan());
  print('===========================');
  print('Titan');
  print('Power point : ' + titan.getPowerPoint().toString());
}
