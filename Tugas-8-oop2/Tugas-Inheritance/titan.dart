// @dart=2.11

class Titan{
  int _powerPoint;

  int getPowerPoint() => _powerPoint;

  setPowerPoint(point){
    if(point <= 5){
      point = 5;
    }
    this._powerPoint = point;
  }
}
