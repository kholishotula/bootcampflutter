import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(HomePage());
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 42.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 60),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(
                  onPressed: () {},
                  icon: Icon(Icons.notifications)
                ),
                IconButton(
                  onPressed: () {},
                  icon: Icon(Icons.add_shopping_cart)
                ),
              ],
            ),
            SizedBox(height: 14),
            Text.rich(
              TextSpan(
                text: "Welcome,",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Color(0xFF54C5F8),
                  fontSize: 40
                ),
                children: <TextSpan>[
                  TextSpan(
                    text: "\nAmaliah",
                    style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.lightBlue[900],
                      fontSize: 36
                    )
                  )
                ]
              ),
            ),
            SizedBox(height: 68),
            TextField(
              decoration: InputDecoration(
                prefixIcon: Padding(
                  padding: EdgeInsets.only(left: 25, right: 16),
                  child: Icon(
                    Icons.search,
                    color: Colors.black,
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Color(0xFF54C5F8))
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Color(0xFF54C5F8))
                ),
                hintText: "Search"
              ),
            ),
            SizedBox(height: 68),
            Text(
              'Recommended Place',
              style: TextStyle(
                fontWeight: FontWeight.normal,
                fontSize: 20
              ),
            ),
            SizedBox(height: 15),
            SizedBox(
              height: 300,
              child: GridView.count(
                padding: EdgeInsets.zero,
                crossAxisCount: 2,
                childAspectRatio: 1.491,
                crossAxisSpacing: 21,
                mainAxisSpacing: 14,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  for (var place in places)
                    Image.asset('assets/img/$place.png')
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

final places = ['Monas', 'Roma', 'Berlin', 'Tokyo'];