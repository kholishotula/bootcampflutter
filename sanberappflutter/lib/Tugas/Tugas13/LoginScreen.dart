import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main(){
  runApp(LoginPage());
}

class LoginPage extends StatelessWidget {
  const LoginPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 41.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 60),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text.rich(
                  TextSpan(
                    text: "Sanber Flutter",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Color(0xFF54C5F8),
                      fontSize: 30,
                    ),
                  )
                )
              ]
            ),
            SizedBox(height: 14),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/img/flutter.png',
                  height: 100,
                  width: 94,
                )
              ],
            ),
            SizedBox(height: 30),
            TextField(
              decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Color(0xFF54C5F8))
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Color(0xFF54C5F8))
                ),
                contentPadding: EdgeInsets.fromLTRB(25, 12, 25, 12),
                hintText: "Username",
                hintStyle: TextStyle(color: Color(0xFF312C2C))
              ),
            ),
            SizedBox(height: 20),
            TextField(
              decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Color(0xFF54C5F8))
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Color(0xFF54C5F8))
                ),
                contentPadding: EdgeInsets.fromLTRB(25, 12, 25, 12),
                hintText: "Password",
                hintStyle: TextStyle(color: Color(0xFF312C2C))
              ),
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Forgot Password',
                  style: TextStyle(color: Colors.lightBlue[400]),
                )
              ],
            ),
            SizedBox(height: 20),
            SizedBox(
              width: double.infinity,
              height: 42,
              child: ElevatedButton(
                onPressed: () {},
                child: Text(
                  'Login',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  Text(
                    'Does not have account?',
                    style: TextStyle(color: Colors.black),
                  )
                ],
              ),
              Column(
                children: [
                  Text(
                    'Sign In',
                    style: TextStyle(color: Colors.lightBlue[400]),
                  )
                ],
              )
            ],
          )
          ],
        ),
      ),
    );
  }
}