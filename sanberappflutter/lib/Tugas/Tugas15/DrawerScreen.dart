import 'package:flutter/material.dart';

class DrawerScreenT15 extends StatefulWidget {
  @override
  _DrawerScreenT15State createState() => _DrawerScreenT15State();
}

class _DrawerScreenT15State extends State<DrawerScreenT15> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text("Kholishotul Amaliah"),
            accountEmail: Text("kholishotula.ka@gmail.com"),
            currentAccountPicture: CircleAvatar(
              backgroundImage: AssetImage("assets/img/maya.jpg"),
            ),
          ),
          DrawerListTileT15(
            iconData: Icons.group,
            title: "New Group",
            onTilePressed: (){},
          ),
          DrawerListTileT15(
            iconData: Icons.lock,
            title: "New Secret Group",
            onTilePressed: (){},
          ),
          DrawerListTileT15(
            iconData: Icons.notifications,
            title: "New Channel Chat",
            onTilePressed: (){},
          ),
          DrawerListTileT15(
            iconData: Icons.contacts,
            title: "Contacts",
            onTilePressed: (){},
          ),
          DrawerListTileT15(
            iconData: Icons.bookmark_border,
            title: "Saved Messages",
            onTilePressed: (){},
          ),
          DrawerListTileT15(
            iconData: Icons.phone,
            title: "Calls",
            onTilePressed: (){},
          ),
        ],
      ),
    );
  }
}

class DrawerListTileT15 extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onTilePressed;

  const DrawerListTileT15({Key key, this.iconData, this.title, this.onTilePressed}) : super(key:key);
  @override 
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(title, style: TextStyle(fontSize: 16),),
    );
  }
}