import 'package:flutter/material.dart';
import 'package:sanberappflutter/Tugas/Tugas15/HomeScreen.dart';
import 'package:sanberappflutter/Tugas/Tugas15/LoginScreen.dart';
import 'package:sanberappflutter/Tugas/Tugas15/ProfileScreen.dart';
import 'package:sanberappflutter/Tugas/Tugas15/SearchScreen.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // jika ingin mengirim argument
    // final args = settings.arguments; 
    switch (settings.name) {
      case '/home':
        return MaterialPageRoute(builder: (_) => HomeScreen());
      case '/login':
        return MaterialPageRoute(builder: (_) => LoginScreen());
      case '/search':
        return MaterialPageRoute(builder: (_) => SearchScreen());
      case '/profile':
        return MaterialPageRoute(builder: (_) => ProfileScreen());
      default:
        return _errorRoute();
    }
  }
  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(title: Text("Error")),
        body: Center(child: Text('Error page')),
      );
    });
  }
}