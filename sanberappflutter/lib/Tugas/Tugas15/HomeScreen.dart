import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sanberappflutter/Tugas/Tugas15/DrawerScreen.dart';

void main() {
  runApp(HomeScreen());
}

class HomeScreen extends StatefulWidget {
  HomeScreen({ Key key }) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  TextEditingController searchController = TextEditingController();

  int currentTab = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Home"),
        actions: <Widget>[
          Padding(padding: const EdgeInsets.all(8.0),
            child: Icon(Icons.search),
          )
        ],
      ),
      drawer: DrawerScreenT15(),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 42.0),
        child: ListView(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 40),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    IconButton(
                      onPressed: () {},
                      icon: Icon(Icons.notifications)
                    ),
                    IconButton(
                      onPressed: () {},
                      icon: Icon(Icons.add_shopping_cart)
                    ),
                  ],
                ),
                SizedBox(height: 14),
                Text.rich(
                  TextSpan(
                    text: "Welcome,",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Color(0xFF54C5F8),
                      fontSize: 40
                    ),
                    children: <TextSpan>[
                      TextSpan(
                        text: "\nAmaliah",
                        style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: Colors.lightBlue[900],
                          fontSize: 36
                        )
                      )
                    ]
                  ),
                ),
                SizedBox(height: 40),
                TextField(
                  controller: searchController,
                  decoration: InputDecoration(
                    prefixIcon: Padding(
                      padding: EdgeInsets.only(left: 25, right: 16),
                      child: Icon(
                        Icons.search,
                        color: Colors.black,
                      ),
                    ),
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xFF54C5F8))
                    ),
                    labelText: "Search"
                  ),
                ),
                SizedBox(height: 40),
                Text(
                  'Recommended Place',
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 20
                  ),
                ),
                SizedBox(height: 15),
                SizedBox(
                  height: 300,
                  child: GridView.count(
                    padding: EdgeInsets.zero,
                    crossAxisCount: 2,
                    childAspectRatio: 1.491,
                    crossAxisSpacing: 21,
                    mainAxisSpacing: 14,
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      for (var place in places)
                        Image.asset('assets/img/$place.png')
                    ],
                  ),
                )
              ],
            ),
          ],
        )
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home'
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: 'Search'
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.people),
            label: 'Profile'
          )
        ],
        currentIndex: currentTab,
        selectedItemColor: Colors.blue,
        unselectedItemColor: Colors.grey,
        showUnselectedLabels: true,
        onTap: (currentTab){
          switch (currentTab) {
            case 1:
              Navigator.pushNamed(context, '/search');
              break;
            case 2:
              Navigator.pushNamed(context, '/profile');
              break;
          }
        },
      ),
    );
  }
}

final places = ['Monas', 'Roma', 'Berlin', 'Tokyo'];
