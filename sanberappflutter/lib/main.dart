import 'package:flutter/material.dart';
import 'package:sanberappflutter/Quiz3/LoginScreen.dart';
import 'package:sanberappflutter/Quiz3/MainApp.dart';
// import 'package:sanberappflutter/Tugas/Tugas15/routes.dart';
// import 'package:sanberappflutter/Tugas/Tugas13/LoginScreen.dart';
// import 'package:sanberappflutter/Tugas/Tugas13/HomeScreen.dart';
// import 'package:sanberappflutter/Tugas/Tugas14/get_data.dart';
// import 'package:sanberappflutter/Tugas/Tugas12/Telegram.dart';

// Tugas 15
// void main() {
//   runApp(MaterialApp(
//     initialRoute: '/login',
//     onGenerateRoute: RouteGenerator.generateRoute,
//   ));
// }

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      // home: Telegram(),
      // home: HomePage(),
      // home: LoginPage()
      // home: GetDataScreen(),

      // Quiz 3
      initialRoute: '/login',
      routes: <String, WidgetBuilder>{
        '/login': (context) => LoginScreen(),
        '/': (context) => MainApp(),
      },

    );
  }
}