// @dart=2.11

import 'dart:io';
import 'lingkaran.dart';

void main(){
  Lingkaran circle;
  double luasLingkaran;

  print('Masukkan jari-jari lingkaran :');
  String jarijari = stdin.readLineSync();

  circle = new Lingkaran();
  circle.setJari(num.parse(jarijari).toDouble());

  luasLingkaran = circle.hitungLuas();
  print('Luas lingkaran : $luasLingkaran');
}
