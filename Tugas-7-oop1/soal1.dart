// @dart=2.11

class Triangle{
  double alas;
  double tinggi;
  double setengah = 0.5;

  double hitungLuas(){
    return this.setengah * this.alas * this.tinggi;
  }
}

void main(){
  Triangle segitiga;
  double luasSegitiga;

  segitiga = new Triangle();
  segitiga.alas = 20.0;
  segitiga.tinggi = 30.0;
  luasSegitiga = segitiga.hitungLuas();

  print(luasSegitiga);
}
