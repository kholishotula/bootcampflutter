// soal 1
teriak(){
  return 'Halo Sanbers!';
}

void main(List<String> args) {
  print(teriak());
}

// soal 2
kalikan(num1, num2){
  return num1 * num2;
}

// void main(List<String> args) {
//   var num1 = 12;
//   var num2 = 4;

//   var hasilKali = kalikan(num1, num2);
//   print(hasilKali);
// }

// soal 3
introduce(name, age, address, hobby){
  return 'Nama saya ' + name + ', umur saya $age tahun, alamat saya di ' + address + ', dan saya punya hobby yaitu ' + hobby + '!';
}

// void main(List<String> args) {
//   var name = "Agus";
//   var age = 30;
//   var address = "Jln. Malioboro, Yogyakarta";
//   var hobby = "Gaming";
  
//   var perkenalan = introduce(name, age, address, hobby);
//   print(perkenalan);
// }

// soal 4
faktorial(num){
  if(num <= 0){
    return 1;
  }
  return num * faktorial(num-1);
}

// void main(List<String> args) {
//   print(faktorial(6));
//   print(faktorial(0));
// }
