import 'dart:io';

void main(List<String> args) {
  // soal 1
  print('Soal 1');

  print('Apakah Anda setuju untuk menginstall aplikasi ini? (y/t)');
  String answer = stdin.readLineSync()!;

  answer.toLowerCase() == 'y' ? print('Anda akan menginstall aplikasi dart') : print('aborted');
  print('============================================');

  // soal 2
  print('Soal 2');

  print('--Werewolf Game--');
  print('');

  print('Masukkan nama : ');
  String nama = stdin.readLineSync()!;
  print('Masukkan peran : ');
  String peran = stdin.readLineSync()!;
  print('');

  if (nama == '' && peran == ''){
    print('Nama harus diisi!');
  }
  else if (peran == ''){
    print('Halo ' + nama + ', Pilih peranmu untuk memulai game!');
  }
  else{
    if(peran.toLowerCase() == 'penyihir'){
      print('Halo ' + peran + ' ' + nama + ', kamu dapat melihat siapa yang menjadi werewolf!');
    }
    else if(peran.toLowerCase() == 'guard'){
      print('Halo ' + peran + ' ' + nama + ', kamu akan membantu melindungi temanmu dari serangan werewolf.');
    }
    else if(peran.toLowerCase() == 'werewolf'){
      print('Halo ' + peran + ' ' + nama + ', Kamu akan memakan mangsa setiap malam!');
    }
  }
  print('============================================');

  // soal 3
  print('Soal 3');

  print('Masukkan nama hari :');
  String hariIni = stdin.readLineSync()!;

  switch (hariIni.toLowerCase()) {
    case 'senin' :
      print('Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.');
      break;
    case 'selasa' :
      print('Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.');
      break;
    case 'rabu' :
      print('Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri.');
      break;
    case 'kamis' :
      print('Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain.');
      break;
    case 'jumat' :
      print('Hidup tak selamanya tentang pacar.');
      break;
    case 'sabtu' :
      print('Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.');
      break;
    case 'minggu' :
      print('Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani.');
      break;
  }
  print('============================================');

  // soal 4
  print('Soal 4');

  var tanggal = 18; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
  var bulan = 8; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
  var tahun = 2021; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

  switch (bulan) {
    case 1:
      print('$tanggal' + ' Januari ' + '$tahun');
      break;
    case 2:
      print('$tanggal' + ' Februari ' + '$tahun');
      break;
    case 3:
      print('$tanggal' + ' Maret ' + '$tahun');
      break;
    case 4:
      print('$tanggal' + ' April ' + '$tahun');
      break;
    case 5:
      print('$tanggal' + ' Mei ' + '$tahun');
      break;
    case 6:
      print('$tanggal' + ' Juni ' + '$tahun');
      break;
    case 7:
      print('$tanggal' + ' Juli ' + '$tahun');
      break;
    case 8:
      print('$tanggal' + ' Agustus ' + '$tahun');
      break;
    case 9:
      print('$tanggal' + ' September ' + '$tahun');
      break;
    case 10:
      print('$tanggal' + ' Oktober ' + '$tahun');
      break;
    case 11:
      print('$tanggal' + ' November ' + '$tahun');
      break;
    case 12:
      print('$tanggal' + ' Desember ' + '$tahun');
      break;  
  }
}