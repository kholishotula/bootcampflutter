import 'dart:io';

void main(List<String> args) {
  var i, j;

  // soal 1
  print('Soal 1');

  i = 0;
  print('LOOPING PERTAMA');
  while (i <= 18){
    i += 2;
    print('$i - I love coding');
  }

  print('LOOPING KEDUA');
  while (i >= 2){
    print('$i - I will become a mobile developer');
    i -= 2;
  }
  print('=========================================');

  // soal 2
  print('Soal 2');

  for (i = 1; i <= 20; i++) {
    if (i % 3 == 0 && i % 2 == 1) {
      print('$i - I Love Coding');
    }
    else if (i % 2 == 1) {
      print('$i - Santai');
    }
    else {
      print('$i - Berkualitas');
    }
  }
  print('=========================================');

  // soal 3
  print('Soal 3');

  for (i = 0; i < 4; i++) {
    for (j = 0; j < 8; j++) {
      stdout.write('#');
    }
    print('');
  }
  print('=========================================');

  // soal 4
  print('Soal 4');

  for (i = 0; i < 7; i++) {
    for (j = 0; j <= i; j++) {
      stdout.write('#');
    }
    print('');
  }
}
